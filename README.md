# Todo-App

A simple To-do application that allows the users write down a list of all they want to do for the day and also allows the user delete the tasks once they have been completed.

# To clone the project

`$ git clone https://gitlab.com/MartianTolu/todo-app.git`

After cloning, cd into directory/folder

`cd todo-app`

# Setup
You will need to install some packages, if they are not yet installed on your machine:

- Node.js (v10.0.0 or higher; LTS)
- optionally, you can install typescript globally.

# Installation

`yarn`

# Running The App

`yarn start`
