import React from 'react';
import TodoPage from './Todo/Todo'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" >
          <TodoPage />
        </Route>
      </Switch>
    </Router>
  )
}

export default App;